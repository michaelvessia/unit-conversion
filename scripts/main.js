function handleFileSelect() {
  var fileSelector = document.getElementById('csv_upload');
  var file = fileSelector.files[0];
  var display = document.getElementById('display');
  if (file) {
    var reader = new FileReader();
    reader.readAsText(file, 'UTF-8');
    reader.onload = function(evt) {
      // parse the csv
      var entries = parse(evt.target.result);
      // get the columns to convert
      var columnsToConvert = getUnitColumns(entries);
      // convert all of the rows
      var entries = convertRows(entries, columnsToConvert);
      // also handle the special case for weight classes
      entries = convertWeightClasses(entries);
      // transform back into a csv
      var csv = Papa.unparse(entries);
      // download the csv for the user
      download('entries.csv', csv);
    }
    reader.onerror = function(evt) {
      display.innerHTML = 'Error reading entries.csv file';
    }
  }
}

// Parse the entries.csv into a data structure where numeric values are converted to numbers from strings
function parse(entries) {
  var result = Papa.parse(entries, {
    dynamicTyping: true
  });
  return result.data;
}

// Given parsed entries.csv, return the columns that contain data to be converted
function getUnitColumns(entries) {
  return entries[0].map(function(column) {
    if (column) {
      return column.includes('LBS');
    }
  });
}

// Get the column index of the weight class column
function getWeightClassColIndex(entries) {
  return entries[0].indexOf('WeightClassKg');
}

// Given headers from parsed entries.csv, return new headers with the unit converted
function convertHeaders(headers, columnsToConvert) {
  return headers.map(function(column, index) {
    if(columnsToConvert[index] || column === 'WeightClassLBS') {
      var regex = /LBS/gi;
      return column.replace(regex, 'Kg');
    }
    return column;
  });
}

// Convert a data row from lbs to kilos, for any columns that we deemed to have Lb values
function convertDataRow(row, columnsToConvert) {
  return row.map(function(col, index) {
    if (columnsToConvert[index]) {
      return lbsToKilos(col);
    } else {
      return col;
    }
  });
}

// Convert all rows, handling header and data rows seperately
function convertRows(entries, columnsToConvert) {
  return entries.map(function(row, rowIndex) {
    var convertRow = rowIndex === 0 ? convertHeaders : convertDataRow;
    var newRow = convertRow(row, columnsToConvert);
    return newRow;
  });
}

// Convert the weight class data column
function convertWeightClasses(entries) {
  var idx = getWeightClassColIndex(entries);
  return entries.map(function(row, rowIndex) {
    if (rowIndex === 0) {
      // If header row, exit, as we already handled this
      return row;
    } else {
      return row.map(function(col, colIndex) {
        if (colIndex === idx) {
          // Convert the weight class
          return convertWeightClass(col);
        }
        // Non-weight class column, leave it
        return col;
      });
    }
  });
}


// Convert lb to kilo value, if 0 then return empty string to conform to opl format
function lbsToKilos(lbs) {
  // Don't try to convert 0 or null values or SHW
  if (lbs === 'SHW') {
    return 'SHW';
  } else if (lbs) {
    var kgs = lbs / 2.20462262;
    // Round to 2 decimal places
    return kgs.toFixed(2);
  } else {
    return '';
  }
}

// Some weight class fixes pulled from fix-weightclasses.py
function convertWeightClass(weightClass) {
  switch(weightClass) {
    case '89.81':
      return '90';
      break;
    case '139.71':
      return '140';
      break;
    case '79.83':
      return '80';
      break;
    case '69.85':
      return '70';
      break;
    case '63.05':
      return '63';
      break;
    case '139.71+':
      return '140+';
      break;
    case '109.77':
      return '110';
      break;
    case '51.71':
      return '52';
      break;
    case '55.69':
      return '56';
      break;
    case '59.87':
      return '60';
      break;
    case '67.13':
      return '67.5';
      break;
    case '74.84':
      return '75';
      break;
    case '74.84+':
      return '75+';
      break;
    case '82.10':
      return '82.5';
      break;
    case '89.81':
      return '90';
      break;
    case '99.79':
      return '100';
      break;
    case '124.74':
      return '125';
      break;
    case '55.79':
      return '56';
      break;
    case '51.98':
      return '52';
      break;
    case '89.81+':
      return '90+';
      break;
    case '82.1':
      return '82.5';
      break;
    case '109.77+':
      return '110+';
      break;
    case '47.63':
      return '48';
      break;
    case '44.00':
      return '44';
      break;
    case '124.74+':
      return '125+';
      break;
    case '124.96':
      return '125';
      break;
    case '99.9':
      return '100';
      break;
    case '89.92':
      return '90';
      break;
    case '47.99':
      return '48';
      break;
    case '59.99':
      return '60';
      break;
    case '74.96':
      return '75';
      break;
    case '82.44':
      return '82.5';
      break;
    case '89.99':
      return '90';
      break;
    case '100.00':
      return '100';
      break;
    case '55.97':
      return '56';
      break;
    case '67.49':
      return '67.5';
      break;
    case '74.98':
      return '75';
      break;
    case '99.97':
      return '100';
      break;
    case '82.46':
      return '82.5';
      break;
    case '110.00':
      return '110';
      break;
    case '125.19+':
      return '125+';
      break;
    case '125.65+':
      return '125+';
      break;
    case '126.10+':
      return '125+';
      break;
    case '126.55+':
      return '125+';
      break;
    case '127.01+':
      return '125+';
      break;
    case '67.50':
      return '67.5';
      break;
    case '88.90':
      return '90';
      break;
    case '47.5':
      return '48';
      break;
    case '67.4':
      return '67.5';
      break;
    case '82':
      return '82.5';
      break;
    case '67':
      return '67.5';
      break;
    case '108.86':
      return '110';
      break;
    case '56.7':
      return '56';
      break;
    case '57.83':
      return '56';
      break;
    case '67.47':
      return '67.5';
      break;
    case '99.90':
      return '100';
      break;
    case '139.82':
      return '140';
      break;
    case '139.98':
      return '140';
      break;
    case '48.08':
      return '48';
      break;
    case '52.44':
      return '52';
      break;
    case '125.19':
      return '125';
      break;
    case '117.48':
      return '117.5';
      break;
    case '139.93':
      return '140';
      break;
    case '55.91':
      return '56';
      break;
    case '51.94':
      return '52';
      break;
    case '44.23':
      return '44';
      break;
    case '89.92+':
      return '90+';
      break;
    case '56.02':
      return '56';
      break;
    case '124.96+':
      return '125+';
      break;
    case '140.16':
      return '140';
      break;
    case '100.24':
      return '100';
      break;
    default:
      return weightClass;
      break;
  }
}

function download(filename, content) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(content));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}
